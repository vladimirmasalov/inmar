import React, {PropTypes, Component} from 'react';
import {
    Button,
    Modal,
    Form,
    FormGroup,
    Col,
    FormControl,
    ControlLabel
} from 'react-bootstrap';

const {func} = PropTypes;
export default class Contacts extends Component {
    static propTypes = {
        onAdd: func,
        onCancel: func
    }

    constructor( props ) {
        super(props);

        this.state = {
            type: '',
            name: '',
            title: '',
            phone: '',
            ext: '',
            fax: '',
            email: ''
        };
    }

    render() {
        return <Modal.Dialog>
            <Modal.Header>
                <Modal.Title>Add New Contact</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <Form horizontal>
                    <FormGroup controlId="formHorizontalType">
                        <Col componentClass={ControlLabel} sm={2}>
                            Type
                        </Col>
                        <Col sm={10}>
                            <FormControl type="text" placeholder="" onChange={ (e) => this.setState( { type: e.target.value} ) } />
                        </Col>
                    </FormGroup>

                    <FormGroup controlId="formHorizontalName">
                        <Col componentClass={ControlLabel} sm={2}>
                            Name
                        </Col>
                        <Col sm={10}>
                            <FormControl type="text" placeholder="" onChange={ (e) => this.setState( { name: e.target.value} ) }/>
                        </Col>
                    </FormGroup>

                    <FormGroup controlId="formHorizontalTitle">
                        <Col componentClass={ControlLabel} sm={2}>
                            Title
                        </Col>
                        <Col sm={10}>
                            <FormControl type="text" placeholder="" onChange={ (e) => this.setState( { title: e.target.value} ) }/>
                        </Col>
                    </FormGroup>

                    <FormGroup controlId="formHorizontalPhone">
                        <Col componentClass={ControlLabel} sm={2}>
                            Phone
                        </Col>
                        <Col sm={8}>
                            <FormControl type="phone" placeholder="" onChange={ (e) => this.setState( { phone: e.target.value} ) }/>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="phone" placeholder="Ext" onChange={ (e) => this.setState( { ext: e.target.value} ) }/>
                        </Col>
                    </FormGroup>

                    <FormGroup controlId="formHorizontalFax">
                        <Col componentClass={ControlLabel} sm={2}>
                            Fax
                        </Col>
                        <Col sm={10}>
                            <FormControl type="phone" placeholder="" onChange={ (e) => this.setState( { fax: e.target.value} ) }/>
                        </Col>
                    </FormGroup>

                    <FormGroup controlId="formHorizontalEmail">
                        <Col componentClass={ControlLabel} sm={2}>
                            Email
                        </Col>
                        <Col sm={10}>
                            <FormControl ref="email" type="email" placeholder="" onChange={ (e) => this.setState( { email: e.target.value} ) }/>
                        </Col>
                    </FormGroup>
                </Form>
            </Modal.Body>

            <Modal.Footer>
                <Button onClick={ this.props.onCancel}>Close</Button>
                <Button
                    bsStyle="primary"
                    onClick={() => { this.props.onAdd( this.state ); }}>Save changes</Button>
            </Modal.Footer>
        </Modal.Dialog>;
    }
}