import React, {PropTypes, Component} from 'react';
import { Table, Button, Glyphicon } from 'react-bootstrap';
import NewContactModal from './NewContact.jsx';

import './Contacts.scss';

// Contacts table Stateless component
const { array, func } = PropTypes;
const ContactsTable = ({ data, onClick }) => {
    return <Table striped condensed hover>
            <thead>
                <tr>
                    <th></th>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Title</th>
                    <th>Phone</th>
                    <th>Ext.</th>
                    <th>Fax</th>
                    <th>Email</th>
                </tr>
            </thead>
            <tbody>
                {
                    data && data.map( (item, index) => {
                        return <tr className='contacts-item' key={`item-${item.id}`}>
                                    <td><input type='checkbox' onClick={ () => onClick( index )} /></td>
                                    <td><span>{item.type}</span></td>
                                    <td><span>{item.name}</span></td>
                                    <td><span>{item.title}</span></td>
                                    <td><span>{item.phone}</span></td>
                                    <td width='55px'><span>{item.ext}</span></td>
                                    <td><span>{item.fax}</span></td>
                                    <td><span>{item.email}</span></td>
                            </tr>;
                    })
                }
            </tbody>
        </Table>;
};

ContactsTable.propTypes = {
    data: array,
    onClick: func
};

//
export default class Contacts extends Component {
    static propTypes = {
        initial: array
    }

    constructor(props) {
        super(props);

        this.state = {
            showModal: false,
            data: props.initial || [],
            totalSelected: 0
        };

        this.onRemove = this.onRemove.bind(this);
        this.onSelect = this.onSelect.bind(this);
        this.addContact = this.addContact.bind(this);
        this.onContactAdd = this.onContactAdd.bind(this);
        this.onContactCancel = this.onContactCancel.bind(this);
    }

    onSelect( index ) {
        const data = this.state.data;
        let selected = this.state.totalSelected;

        if ( data[index] ) {
            data[index].selected = !data[index].selected;
            selected += data[index].selected ? 1 : -1;
        }
        this.setState( { data: [...data], totalSelected: selected} );
    }

    addContact() {
        this.setState( {showModal: true} );
    }

    onContactCancel() {
        this.setState( {showModal: false} );
    }

    onContactAdd( { type, name, title, phone, ext, fax, email } ) {
        const data = [ ...this.state.data, { type, name, title, phone, ext, fax, email } ];
        this.setState( {data} );
        this.setState( {showModal: false} );
    }

    onRemove() {
        const data = [ ...this.state.data.filter( (obj) => !obj.selected ) ];
        this.setState( {data} ); 
    }

    render() {
        return <section className='contacts'>
                    <a role='button' className='pull-right' data-toggle='collapse' data-target='#contacts-table-wrapper'>
                        <span className='box-icon'>_</span>
                        <span className='text'>Collapse</span>
                    </a>
                    <h4>External Contacts</h4>
                    <h6>Select the client contacts associated with this offer</h6>
                    <div id='contacts-table-wrapper' className='tableWrapper collapse in' aria-expanded='true'>
                    {
                        <ContactsTable data={ this.state.data } onClick={ this.onSelect } />
                    }
                    </div>
                    <Button bsStyle='primary' onClick={ this.addContact }><Glyphicon glyph='plus'></Glyphicon>Add Contact</Button>
                    <Button bsStyle='danger' onClick={ this.onRemove } disabled={ this.state.totalSelected <= 0}><Glyphicon glyph='trash'></Glyphicon>Remove data</Button>

                    { this.state.showModal &&
                        <NewContactModal onAdd={ this.onContactAdd } onCancel={ this.onContactCancel } />
                    }
                </section>;
    }
}


