import React, {Component} from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import Contacts from './../contacts/Contacts.jsx';

import './Main.scss';

const data = {
    'contacts': [
        { 'id': 1, 'type': 'Executive', 'name': 'Ann Brown', 'title': 'CEO', 'phone': '(512) 456-5555', 'ext': '', 'fax': '(512) 456-5555', 'email': 'Executive'},
        { 'id': 2, 'type': 'Inmar AR', 'name': 'Mary Smith', 'title': 'Lorem Ipsum', 'phone': '(512) 456-5555', 'ext': '', 'fax': '(512) 456-5555', 'email': 'Inmar AR'},
        { 'id': 3, 'type': 'Executive', 'name': 'Jhon Doe', 'title': 'Dolor Sit', 'phone': '(512) 456-5555', 'ext': '', 'fax': '(512) 456-5555', 'email': 'Executive'},
        { 'id': 4, 'type': 'Daily', 'name': 'Jhon Doe', 'title': 'Dolor sit amet', 'phone': '(512) 456-5555', 'ext': '', 'fax': '(512) 456-5555', 'email': 'Daily'},
        { 'id': 5, 'type': 'Other', 'name': 'Jhon Doe', 'title': 'Lorem Ipsum', 'phone': '(512) 456-5555', 'ext': '', 'fax': '(512) 456-5555', 'email': 'Other'}
    ]
};

export default class Main extends Component {
    constructor( props ) {
        super(props);
    }

    render() {
        return <Grid className='main' fluid={true}>
                    <Row>
                        <Col xs={12}>
                            <Contacts initial={ data.contacts } />
                        </Col>
                    </Row>
               </Grid>;
    }
}