import React from 'react';
import Main from './main/Main.jsx';

/**
 * App stateless component acting as a wrapper for user content.
 * Here should be added header / footers and other layout elements
 */
const App = () => {
    return <section> 
                <Main /> 
            </section>;
};

export default App;