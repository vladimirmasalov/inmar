const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin');
const InlineManifestPlugin = require('inline-manifest-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const { removeEmpty } = require('webpack-config-utils');
const resolve = require('path').resolve;

const APP = resolve('./src/client');
const BUILD = resolve('./build/production');
const TEMPLATE = resolve(`${APP}/templates/index.html`);
const NODE_MODULES = resolve('node_modules');
const PACKAGE = Object.keys(require(resolve('./package.json')).dependencies);

module.exports = () => {
    return {
        entry: { 
            app: ['babel-polyfill', `${APP}/index.jsx`],
            vendor: PACKAGE
        },

        output: { 
            path: BUILD,
            filename: '[name]-[chunkhash].min.js',
            chunkFilename: '[name]-[chunkhash].min.js',
            publicPath: '/',
            pathinfo: false
        },

        module: {
            rules: [
                {
                    test: /\.(jsx|js)?$/,
                    use: ['babel-loader?cacheDirectory'],
                    include: APP
                }, {
                    test: /\.(css|scss)$/,
                    use: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            {
                            'loader': 'css-loader?importLoaders=1'
                            },
                            {
                                loader: 'postcss-loader',
                                options: {
                                    plugins: function() {
                                        return [
                                            require('postcss-easy-import'),
                                            require('postcss-mixins'),
                                            require('precss'),
                                            require('autoprefixer')
                                        ];
                                    }
                                }
                            }
                        ]
                    }),
                    include: [APP, NODE_MODULES]
                }, { 
                    test: /\.(woff|ttf|eot|svg)?$/, 
                    loader: 'url-loader?limit=100000'
                }
            ]
        },
        plugins: removeEmpty([
            new ProgressBarPlugin(),
            new CleanWebpackPlugin( BUILD ),
            new LodashModuleReplacementPlugin(),
            new webpack.DefinePlugin({
                __DEV_MODE__: false,
                __DEVTOOLS__: false,
                'process.env': {
                    NODE_ENV: JSON.stringify( 'production' )
                }
            }),
            new ExtractTextPlugin({
                filename: '[name]-[chunkhash].min.css',
                allChunks: true
            }),
            new HtmlWebpackPlugin({
                template: TEMPLATE,
                inject: 'body',
                minify: {
                        collapseWhitespace: true,
                        miniftJS: true,
                        useShortDoctype: true,
                        removeComments: false,
                        processConditionalComments: true,
                        conservativeCollapse: true
                }
            }),
            new InlineManifestPlugin(),
            new webpack.optimize.CommonsChunkPlugin({
                names: ['vendor', 'manifest']
            }),
            new webpack.ContextReplacementPlugin(),
            new webpack.LoaderOptionsPlugin({
                minimize: true
            }),
            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    warnings: false
                }
            }),
            new CompressionPlugin({
                asset: '[path].gz[query]',
                algorithm: 'gzip',
                test: /\.js$|\.html$/,
                threshold: 0,
                minRatio: 0
            })
        ])
    };
};