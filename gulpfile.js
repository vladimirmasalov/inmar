const gulp = require('gulp');
const gUtil = require('gulp-util');
const webpack = require('webpack');
const webpackConfigDev = require('./webpack.dev.js')();
const webpackConfigProd = require('./webpack.prod.js')();

gulp.task('webpack:develop', callback => {
    webpack(webpackConfigDev, (error, stats) => {
        if (error) {
            throw new gUtil.PluginError('webpack', error);
        } else {
            gUtil.log('[webpack]', stats.toString( {
                colors: true,
                reasons: true,
                chunks: false
            }));
        }
        callback();
    });
});

gulp.task('webpack:production', callback => {
    webpack(webpackConfigProd, (error, stats) => {
        if (error) {
            throw new gUtil.PluginError('webpack', error);
        } else {
            gUtil.log('[webpack]', stats.toString( {
                colors: true,
                chunks: false
            }));
        }
        callback();
    });
});

gulp.task('default', ['webpack:production']);
gulp.task('develop', ['webpack:develop']);