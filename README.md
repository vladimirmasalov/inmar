# Installation

You need to have a nodejs server installed. See [nodejs official web site](https://nodejs.org/en/)

Once you have nodejs installed. Run the following command
```
npm install -g yarn
```

Navigate to the project folder and install deps.
```
yarn install
```

I personally prefer `yarn` as it have multiple advantages over `npm`.
But you can install everythin using `npm install` command as well.

# Building & Running project

Before you run the project, you need to 'compile' it first. 
Run ``npm run build``. When project is compiled you can run it.
```
npm start
```
> It will start local server on 8080 port. As an option start the server and click this [link](http://localhost:8080)