const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const {removeEmpty} = require('webpack-config-utils');
const resolve = require('path').resolve;

const APP = resolve('./src/client');
const BUILD = resolve('./build/develop');
const TEMPLATE = resolve(`${APP}/templates/index.html`);
const NODE_MODULES = resolve('node_modules');
const LINT = resolve(process.cwd(), '.eslintrc.json');

module.exports = () => {
    return {
        entry: { 
            app: ['babel-polyfill',
                  `${APP}/index.jsx`]
        },
        output: { 
            path: BUILD,
            filename: '[name].js',
            publicPath: '/'
        },

        devtool: 'eval-source-map',
        devServer: {
            historyApiFallback: true,
            hot: true,
            stats: 'errors-only',
            host: 'localhost',
            port: '8000'
        },

        module: {
            rules: [
                {
                    test: /\.(jsx|js)?$/,
                    loaders: 'eslint-loader',
                    enforce: 'pre',
                    include: APP,
                    options: {
                        configFile: LINT,
                        emitError: true
                    }
                },
                {
                    test: /\.(jsx|js)?$/,
                    use: ['babel-loader'],
                    include: APP
                }, {
                    test: /\.(css|scss)$/,
                    use: [
                        {
                            'loader': 'style-loader'
                        },
                        {
                            'loader': 'css-loader?importLoaders=1'
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: function() {
                                    return [
                                        require('postcss-easy-import'),
                                        require('postcss-mixins'),
                                        require('precss'),
                                        require('autoprefixer')
                                    ];
                                }
                            }
                        }
                    ],
                    include: [APP, NODE_MODULES]
                }, { 
                    test: /\.(woff|ttf|eot|svg)?$/, 
                    loader: 'url-loader?limit=100000'
                }
            ]
        },
        devtool: 'eval-source-map',
        plugins: removeEmpty([
            new ProgressBarPlugin(),
            new CleanWebpackPlugin(BUILD),
            new webpack.DefinePlugin({
                __DEV_MODE__: true,
                __DEVTOOLS__: true,
                'process.env': {
                    'NODE_ENV': JSON.stringify('development')
                }
            }),
            new webpack.HotModuleReplacementPlugin(),
            new HtmlWebpackPlugin({
                template: TEMPLATE,
                inject: 'body'
            }),
            new webpack.ContextReplacementPlugin()
        ])
    };
};